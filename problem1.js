const path = require('node:path')

const fs = require('node:fs').promises

function creation(limit) {

  fs.mkdir('./jsonfiles')
    .then(() => console.log("directory created successfully "))
    .then(() => {
      for (let index = 0; index < limit; index++) {
        fs.writeFile(path.join('./jsonfiles', `sample${index + 1}.json`), "hello")
        console.log(`file created.`)
      }
    })
    .then(() => {
      for (let index = 0; index < limit; index++) {
        fs.unlink(path.join('./jsonfiles', `sample${index + 1}.json`))
        console.log("file deleted successfully")
      }
    })
    .catch((err) => console.error(`Error creating directory ${err}`))

}
module.exports = creation
