const path = require('node:path')

const fsPromise = require('node:fs').promises

function operations() {

    fsPromise.readFile('../lipsum.txt', 'utf-8').then((data) => {
        return data
    }).then((upper) => {
        fsPromise.writeFile('./upper.txt', JSON.stringify(upper.toUpperCase())).then(() => {
            console.log("uppercasedata written successfully")
        })
    }).then(() => {
        fsPromise.appendFile('./filenames.txt', 'upper.txt\n')
    }).then(() => {
        return fsPromise.readFile('./upper.txt', 'utf-8')
    }).then((data) => {
        fsPromise.writeFile('./lower.txt', JSON.stringify(data.toLowerCase().split('.'))).then(() => {
            console.log("lowercasedata written successfully")
        })
    }).then(() => {
        fsPromise.appendFile('./filenames.txt', 'lower.txt\n')
    }).then(() => {
        return fsPromise.readFile('./lower.txt', 'utf-8')
    }).then((data) => {
        const sorted = data.split(',').sort()
        fsPromise.writeFile('./sorted.txt', sorted).then(() => {
            console.log("sorteddata written successfully")
        })
    }).then(() => {
        fsPromise.appendFile('./filenames.txt', 'sorted.txt\n')
    }).then(() => {
        fsPromise.readFile('./filenames.txt', 'utf-8').then((data) => {
            data.split('\n').forEach((each) => {
                if (each != "") {
                    fsPromise.unlink(each)
                }
            })
        })
    })
}
module.exports = operations
